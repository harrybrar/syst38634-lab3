package complexPassword;

import java.io.Serializable;

public class Password {

	public static void main(String[] args) 
	{
	 
	}
	
	public static Boolean checkPasswordLength(String password) throws StringIndexOutOfBoundsException {
		
		if (password.length() <= 0)
		{
			throw new StringIndexOutOfBoundsException("No input for password detected");
		}
		
		if(password.length() >= 8) {
			return true;
		}
		return false;
	}
	
	
	public static Boolean checkPasswordDigits(String password) throws StringIndexOutOfBoundsException {
		if (password.length() <= 0)
		{
			throw new StringIndexOutOfBoundsException("No input for password detected");
		}
		int numberofDigits = 0;
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				numberofDigits++;
				if(numberofDigits >=2)
					return true;
			}
		}
		
		return false;
	}

}
