package complexPassword;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class PasswordTest {
	
	private String passwordToTest;
	
	public PasswordTest(String password) {
		passwordToTest = password;
	}

    @Parameterized.Parameters
    public static Collection<Object [ ]> loadData( ) {

    Object [ ][ ] data = {{""}};
    return Arrays.asList( data );
    }
    /*
	@Test
	public void testPasswordLength() {
		
		System.out.print("Testing Password Length Criteria");
		Password password = new Password();
		assertEquals(true, password.checkPasswordLength(this.passwordToTest));
	}
	
	
	@Test
	public void testPasswordDigits() {			
		System.out.print("Testing Password Digit Criteria");
		Password password = new Password();
		assertEquals(true, password.checkPasswordDigits(this.passwordToTest));
	}
	*/
	
    @Test(expected = StringIndexOutOfBoundsException.class)
    public void testException() throws Exception {
        System.out.println("Exception Testing");
        Password password = new Password();
        password.checkPasswordDigits(this.passwordToTest);
    }

}
